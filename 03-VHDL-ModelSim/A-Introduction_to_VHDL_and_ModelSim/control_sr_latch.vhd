library ieee;
use ieee.std_logic_1164.all;

entity control_sr_latch is
	port(
		C, S, R : in std_logic;
		Q, Qnot : inout std_logic
	);
end control_sr_latch;

architecture arch of control_sr_latch is
begin
	Q <= (S NAND C) NAND Qnot;
	Qnot <= (R NAND C) NAND Q;
end arch;