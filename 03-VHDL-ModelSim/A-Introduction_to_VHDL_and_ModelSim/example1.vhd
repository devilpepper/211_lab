entity example1 is
	port(
		x1, x2, x3 : in bit;
		f : out bit
	);
end example1;

architecture logicfunc of example1 is
begin
	f<=(x1 AND x2) OR (NOT x2 AND x3);
end logicfunc;
