entity example2 is
	port(
		x1, x2, x3, x4 : in bit;
		f,g : out bit
	);
end example2;

architecture logicfunc of example2 is
begin
	f <= (x1 AND x3) OR (x2 AND x4);
	g <= (x1 OR NOT x3) AND (NOT x2 OR x4);
end logicfunc;