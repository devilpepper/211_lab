LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY jk_FF IS
  PORT (
    J, K, C : IN STD_LOGIC;
    Q, Qnot : INOUT STD_LOGIC 
  );
END jk_FF;

ARCHITECTURE arch OF jk_FF IS
  COMPONENT msd_flipflop
    Port(
      Input, Clock : IN STD_LOGIC;
      outQ, outQnot : INOUT STD_LOGIC 
    );
  END COMPONENT;
  SIGNAL jnk: STD_LOGIC;
BEGIN
  jnk <= (J AND Qnot) OR ((NOT K) AND Q);
  jk0: msd_flipflop PORT MAP( Input => jnk, Clock => C, outQ => Q, outQnot => Qnot );
END arch;
