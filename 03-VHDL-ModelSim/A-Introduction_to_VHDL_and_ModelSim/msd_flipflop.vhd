LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY msd_flipflop IS
  PORT (
    Input, Clock : IN STD_LOGIC;
    outQ, outQnot : INOUT STD_LOGIC 
  );
END msd_flipflop;

ARCHITECTURE arch OF msd_flipflop IS
  -- our master/slave ff is built
  -- with D latches from previous exercise.
  -- Here is reference to *some* d-latch,
  -- you must already have it coded in a separate d_latch.vhd file
  COMPONENT d_latch
    Port(
      C, D : IN STD_LOGIC;
      Q, Qnot : INOUT STD_LOGIC 
    );
  END COMPONENT;
  SIGNAL qd, notClk: STD_LOGIC;
BEGIN
  notClk <= not Clock;
  FF1: d_latch PORT MAP( D => Input, C => notClk, Q => qd);
  FF2: d_latch PORT MAP( d => qd, C => Clock, Q => outQ, Qnot => outQnot );
END arch;