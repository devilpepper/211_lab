LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY msd_flipflop_behavioral IS
  PORT( 
    D, Clock : IN STD_LOGIC;
    Q, Qnot : OUT STD_LOGIC
  );
END msd_flipflop_behavioral;

ARCHITECTURE Behavior OF msd_flipflop_behavioral IS
BEGIN
  -- We model the output of Q at a rising clock event
  -- No need to describe the config of each D-latch
  PROCESS(Clock)
  BEGIN
    IF (Clock'EVENT AND Clock = '1') THEN
      Q <= D;
      Qnot <= NOT D;
    END IF;
  END PROCESS;
END Behavior;