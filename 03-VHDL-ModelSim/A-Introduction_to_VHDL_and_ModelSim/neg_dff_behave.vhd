
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY neg_dff_behave IS
  PORT( 
    D, Clock : IN STD_LOGIC;
    Q, Qnot : OUT STD_LOGIC
  );
END neg_dff_behave;

ARCHITECTURE Behavior OF neg_dff_behave IS
BEGIN
  -- We model the output of Q at a falling clock event
  -- No need to describe the config of each D-latch
  PROCESS(Clock)
  BEGIN
    IF (Clock'EVENT AND Clock = '0') THEN
      Q <= D;
      Qnot <= NOT D;
    END IF;
  END PROCESS;
END Behavior;