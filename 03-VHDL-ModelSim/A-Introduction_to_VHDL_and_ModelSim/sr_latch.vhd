library ieee;
use ieee.std_logic_1164.all;

entity sr_latch is
	port(
		S, R : in std_logic;
		Q, Qnot : inout std_logic
	);
end sr_latch;

architecture arch of sr_latch is
begin
	Q <= R nor Qnot;
	Qnot <= S nor Q;
end arch;