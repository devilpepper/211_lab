LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY t_FF IS
  PORT (
    T, C : IN STD_LOGIC;
    Q, Qnot : INOUT STD_LOGIC 
  );
END t_FF;

ARCHITECTURE arch OF t_FF IS
  COMPONENT msd_flipflop
    Port(
      Input, Clock : IN STD_LOGIC;
      outQ, outQnot : INOUT STD_LOGIC 
    );
  END COMPONENT;
  SIGNAL tnt : STD_LOGIC;
BEGIN
  tnt <= (T AND Qnot) OR ((NOT T) AND Q);
  t0: msd_flipflop PORT MAP( Input => tnt, Clock => C, outQ => Q, outQnot => Qnot );
END arch;

