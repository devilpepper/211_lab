library ieee;
use ieee.std_logic_1164.all;

entity addsub8 is
	port(Cin : in std_logic;
	X, Y : in std_logic_vector(7 downto 0);
	S : out std_logic_vector(7 downto 0);
	Cout : out std_logic);
end addsub8;

architecture structure of addsub8 is
	signal C : std_logic_vector(1 to 7);
	signal sign : std_logic_vector(7 downto 0);
	component fulladder
		port(Cin, x, y : in std_logic;
				s, Cout : out std_logic);
	end component;
begin
	sign(0) <= Y(0) XOR Cin;
	sign(1) <= Y(1) XOR Cin;
	sign(2) <= Y(2) XOR Cin;
	sign(3) <= Y(3) XOR Cin;
	sign(4) <= Y(4) XOR Cin;
	sign(5) <= Y(5) XOR Cin;
	sign(6) <= Y(6) XOR Cin;
	sign(7) <= Y(7) XOR Cin;
	s0: fulladder port map(Cin, X(0), sign(0), S(0), C(1));
	s1: fulladder port map(C(1), X(1), sign(1), S(1), C(2));
	s2: fulladder port map(C(2), X(2), sign(2), S(2), C(3));
	s3: fulladder port map(C(3), X(3), sign(3), S(3), C(4));
	s4: fulladder port map(C(4), X(4), sign(4), S(4), C(5));
	s5: fulladder port map(C(5), X(5), sign(5), S(5), C(6));
	s6: fulladder port map(C(6), X(6), sign(6), S(6), C(7));
	s7: fulladder port map(C(7), X(7), sign(7), S(7), Cout);
end structure;