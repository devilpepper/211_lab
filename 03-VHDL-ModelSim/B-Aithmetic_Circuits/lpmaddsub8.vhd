library ieee;
use ieee.std_logic_1164.all;

entity lpmaddsub8 is
	generic (n : integer:=8);
	port (Cin : in std_logic;
			X, Y : in std_logic_vector(n-1 downto 0);
			S : out std_logic_vector(n-1 downto 0);
			Cout: out std_logic);
end lpmaddsub8;

architecture structure of lpmaddsub8 is
	signal C : std_logic;
	component lpmaddsub
		port(add_sub : in std_logic;
			  dataa, datab : in std_logic_vector(7 downto 0);
			  result : out std_logic_vector(7 downto 0);
			  cout: out std_logic);
	end component;
begin
	C <= NOT Cin;
	s0: lpmaddsub port map(C, X, Y, S, Cout);
end structure;