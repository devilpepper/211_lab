-- lfsr.vhd
-- This is the code for (Fibonacci) linear shift register
-- make sure you complete the TO DO areas.
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lfsr is
	PORT (
		clk : in std_logic; --TO DO: define port input;
		reset : in std_logic;--TO DO: define reset port input;
		lfsr_out : out std_logic_vector(7 downto 0)--TO DO: define lfsr vector port output
		);
END ENTITY;

ARCHITECTURE behavioral OF lfsr IS
	--This line gives a seed value of 1 to avoid lockup:
	SIGNAL lfsr_tmp:std_logic_vector (7 DOWNTO 0) := (0=>'1', OTHERS=>'0');
	--This code defines the "taps".
	--These are the bit positions that will be picked
	--by XOR gates to generate the next number in the sequence:
	CONSTANT polynome :std_logic_vector (0 to 7) := "10111000";
BEGIN
	PROCESS (clk, reset)
	VARIABLE feedback :std_logic;
	BEGIN
		feedback := lfsr_tmp(0);
		FOR i IN 1 TO 7 LOOP
			feedback := feedback xor ( lfsr_tmp(i) and polynome(i) );
		END LOOP;
		IF (reset = '1') THEN
			lfsr_tmp <= (0=>'1', OTHERS=>'0');
		ELSIF (RISING_EDGE(clk)) THEN
			lfsr_tmp <= feedback & lfsr_tmp(7 DOWNTO 1);
		END IF;
	END PROCESS;
	lfsr_out <= lfsr_tmp;
END ARCHITECTURE;