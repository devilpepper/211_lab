To, Location
clk, PIN_G26
reset, PIN_P23
addy[2], PIN_P25
addy[1], PIN_N26
addy[0], PIN_N25
WE, PIN_N23
CS, PIN_V2
OE, PIN_V1
lfsrOut[7], PIN_Y18
lfsrOut[6], PIN_AA20
lfsrOut[5], PIN_U17
lfsrOut[4], PIN_U18
lfsrOut[3], PIN_V18
lfsrOut[2], PIN_W19
lfsrOut[1], PIN_AF22
lfsrOut[0], PIN_AE22
output[7], PIN_AC21
output[6], PIN_AD21
output[5], PIN_AD23
output[4], PIN_AD22
output[3], PIN_AC22
output[2], PIN_AB21
output[1], PIN_AF23
output[0], PIN_AE23