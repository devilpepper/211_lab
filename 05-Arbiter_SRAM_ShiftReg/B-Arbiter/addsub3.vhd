library ieee;
use ieee.std_logic_1164.all;

entity addsub3 is
	port(Cin : in std_logic;
	X, Y : in std_logic_vector(2 downto 0);
	S : out std_logic_vector(2 downto 0);
	Cout : out std_logic);
end addsub3;

architecture structure of addsub3 is
	signal C : std_logic_vector(1 to 2);
	signal sign : std_logic_vector(2 downto 0);
	component fulladder
		port(Cin, x, y : in std_logic;
				s, Cout : out std_logic);
	end component;
begin
	sign(0) <= Y(0) XOR Cin;
	sign(1) <= Y(1) XOR Cin;
	sign(2) <= Y(2) XOR Cin;
	s0: fulladder port map(Cin, X(0), sign(0), S(0), C(1));
	s1: fulladder port map(C(1), X(1), sign(1), S(1), C(2));
	s2: fulladder port map(C(2), X(2), sign(2), S(2), Cout);
end structure;