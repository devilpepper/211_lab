LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY d_latch IS
  PORT (
		C, D : in std_logic;
		Q, Qnot : inout std_logic
	);
END d_latch;

ARCHITECTURE arch OF d_latch IS
BEGIN
  Q <= (D NAND C) NAND Qnot;
  Qnot <= ((NOT D) NAND C) NAND Q;
END arch;